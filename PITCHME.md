@title[COVER]
## Business Information Technology


---
@title[Tools]
### Got tools?

<ol>
<li class="fragment">MS Sway</li>
<li class="fragment">Prezi</li>
<li class="fragment">GitPitch</li>
</ol>

+++
@title[GitPitch]
### GitPitch
- OpenSource 
- Markdown code 
- Fast and lightweight

---

@title[Topic]
### Got topic?

<span class="fragment" style="font-size: 0.8em; color:gray">(sport is always a good call)</span>


+++
![Video](https://www.youtube.com/embed/TMIGc-PqsBc)
---
@title[Data]
### Got data?

<span class="fragment" style="font-size: 0.8em; color:gray">Let's scrape some data with R... Wait, what? </span>

+++
#### Data scraping from HTML

```R
library(rvest)
url <- 'http://www.powerlifting-ipf.com/fileadmin/ipf/data/results/2017/classic-powerlifting/detailed_scoresheet_m.htm'
webpage <- read_html(url)
```
<span class="fragment" style="font-size: 0.8em; color:gray">Cons: site should have decent CSS structure</span>

+++
#### OpenPowerlifting Data

* https://github.com/sstangl/openpowerlifting

"We strongly encourage federations to stop using PDF."

---

<iframe width="800" height="600" src="https://app.powerbi.com/view?r=eyJrIjoiOWNmMWJiOWEtNDA2Mi00NDAyLThjZTktNGZlZmQzMjQyYzYyIiwidCI6IjM0MTVmMmY3LWY1YTgtNDA5Mi1iNTJhLTAwM2FhZjg0NDg1MyIsImMiOjh9" frameborder="0" allowFullScreen="true"></iframe>

+++
#### Normalization using WILKS method
```R
wilks <- function(x) 500/(a+b*x+c*x**2+d*x**3+e*x**4+f*x**5)
curve(wilks, 50, 200, xname = "Bodyweight")
```
![Logo](/pics/wilks.png)

+++

*Validation of the Wilks powerlifting formula: 1996 and 1997 World Championships (a total of 30 men and 27 women for each lift)*

* no bias for men's or women's BP and TOT
* favorable bias toward intermediate weight class lifters in the women's SQ 
* linear unfavorable bias toward heavier men and women in the DL

+++
#### Other drawbacks:

* Bias against middleweights
* Calculated for EQ lifts
* Calculated for totals

+++?image=/pics/corr.png
+++
#### Pipelines with R

```R
left_join(powerlifting, meets, by = "MeetID") %>%
  filter(Federation == "IPF" & Equipment == "Raw" & !is.na(BestSquatKg) & !is.na(BestBenchKg) & !is.na(BestDeadliftKg)) %>%
  select(Age, BodyweightKg, BestSquatKg, BestBenchKg, BestDeadliftKg, TotalKg, Wilks) %>%
  chart.Correlation(pch=19)
```
---
### Open questions
<ol>
<li class="fragment">Lifters progress during the years (animated)</li>
<li class="fragment">Validate WILKS formula using newest data of RAW lifts</li>
<li class="fragment">Compare WILKS formula with other coefficients (Glossbrenner (WPC), Reshel (GPC, GPA, WUAP, IRP))</li>
<li class="fragment">Find ultimate method to determine the best lifter</li>
</ol>
---
#### References:

* https://physical-preparedness.github.io/wilks-validation/ 
* https://github.com/sstangl/openpowerlifting
* https://github.com/tidyverse/tidyverse
* https://www.ncbi.nlm.nih.gov/pubmed/10613442
* https://www.strongerbyscience.com/whos-the-most-impressive-powerlifter/

--- 
#### Got questions?

![Logo](/pics/KK.png)




